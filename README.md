# get_pix

> download image files from harvard art museum

## setup

### requirements

- python ^3.9
  - `brew install python@latest`
- poetry
  - `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`
- [imgcat](https://github.com/eddieantonio/imgcat)
  - `brew install imgcat`
### installation

1) git clone this repository
```bash
git clone git@gitlab.com:Hp4k1h5/get_pix.git
```
2) poetry
```
cd get_pix
poetry install
```

## usage

### examples

from the `get_pix` repository

```bash
# print help
./bin/__main__.py -h

# get 10 records of monet and all associated images
./bin/__main__.py --query "monet" 

# get 100 records of van gogh and all associated images
./bin/__main__.py -q "van gogh" --end 100 

# get records 10-30 of early minoan art and all associated images
./bin/__main__.py -q "minoan" -s 10 -e 30 

# get the 100th record of art and all associated images
./bin/__main__.py -q "art" -s 100 -e 101 

# get 100 records of art in batches of 20
./bin/__main__.py -q "art" -s 0 -e 100 --batch_size 20
```

### TODO

- tests
- setup
- brew
