import argparse

parser = argparse.ArgumentParser(description="Process some integers.")

parser.add_argument(
    "-q", "--query", dest="query", default="", help="query to search pictures"
)

parser.add_argument(
    "-s",
    "--start",
    dest="start",
    default=0,
    type=int,
    help="which picture in the result set to start from",
)
parser.add_argument(
    "-e",
    "--end",
    dest="end",
    default=10,
    type=int,
    help="last picture in the result set to download",
)

parser.add_argument(
    "-b",
    "--batch_size",
    dest="batch_size",
    default=10,
    type=int,
    help="number of artwork records to fetch at a time",
)

args = parser.parse_args()
