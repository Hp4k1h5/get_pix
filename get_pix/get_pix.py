import os
import re
from pathlib import Path
from time import sleep
import math

import requests

BASE_URL = "https://harvardartmuseums.org/browse"


def get_records(query: str, offset: int, batch_size: int):
    search = {
        "q": query,
        "load_amount": batch_size,
        "offset": offset * batch_size,
    }
    print("searching", search)

    sleep(3)
    try:
        response = requests.get(BASE_URL, params=search)
        return response.json()
    except Exception as e:
        print(e)


def filter_records(records):
    """
    extracts metadata (title, author, img urls) from records,
    and filters out non image-bearing records
    """
    filtered_records = []
    # iterate over records
    for record in records:
        if not record.get("images"):
            print("no images", record)
            continue

        author = ""
        if record.get("people"):
            author = record["people"][0]
            if author:
                author = make_filename(author["alphasort"])
        title = record["title"]
        img_urls = map(
            lambda image: {
                "url": image["baseimageurl"],
                "ft": image["format"].split("/")[1],
                "imageid": image["imageid"],
                "author": author,
            },
            record["images"],
        )

        # add each record to return val
        filtered_records.append({"title": title, "img_urls": list(img_urls)})

    return filtered_records


def make_filename(text):
    """
    strips all non word-chars from input <text>
    and replaces them with underscores
    """
    return re.sub(r"\W+", "_", text)


def download_imgs(records, search):
    # make a directory for each query
    path_to_file = Path(__file__)
    path_to_file = path_to_file.parent.parent.absolute()
    prefix = make_filename(search if len(search) else "_")
    fp = f"{path_to_file}/imgs/{prefix}"
    os.makedirs(fp, exist_ok=True)

    # iterate over records
    for record in records:
        # Iterate over image urls in each artwork record.
        # Some artworks have more than one image
        for url in record["img_urls"]:
            title = make_filename(record["title"])
            # construct filename including filepath
            fn = f'{fp}/{url["author"]}_{title}-{url["imageid"]}.{url["ft"]}'
            if os.path.isfile(fn):
                # print existing image
                os.system(f"imgcat {fn}")
                # don't redownload existing images
                continue

            # sleep to not draw attn from servers
            sleep(7)
            try:
                r = requests.get(url["url"])

                # write img to its query directory
                with open(fn, "wb") as f:
                    f.write(r.content)
                    print("wrote ", fn)

                # print img to terminal; must have imgcat
                # https://github.com/eddieantonio/imgcat
                os.system(f"imgcat {fn}")
            except Exception as e:
                print(e)


def run(args):
    search = args.query
    batch_size = min(args.batch_size, args.end - args.start)
    current_batch = math.floor(args.start / batch_size)
    max_offset = math.ceil(args.end / batch_size)

    # main loop
    for batch in range(current_batch, max_offset):
        # get artwork records
        response = get_records(search, batch, batch_size)

        # filter/map records to get metadata
        filtered_records = filter_records(response["records"])

        # download imgs for each batch
        download_imgs(filtered_records, search)

        # check each response for end of search results
        if not response["info"].get("next"):
            print("all done")
            break
