#!/usr/bin/env poetry run python

if __name__ == "__main__":

    from arg_parser.arg_parser import args

    from get_pix.get_pix import run

    run(args)
